﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core;
using Rikka.S5.S5Core.Messages;

namespace Rikka.S5.S5Node
{
    /// <summary>
    /// Готовый класс для запуска узла сети
    /// </summary>
    public class NodeInstance: INodeApi
    {
        /// <summary>
        /// конфиг, загружается и сохраняется в xml
        /// </summary>
        public NodeConfing Config { get; set; }

        /// <summary>
        /// роли установленные на этом узле
        /// </summary>
        public List<I5Role> Roles { get; set; }

        /// <summary>
        /// информация по текущему узлу
        /// </summary>
        public NodeInstanceInformarion NodeInstanceInformarion { get; set; }

        /// <summary>
        /// доступно только для контролера сети
        /// </summary>
        public S5Controller Controller { get; set; }

        public const int Port = 4632;

        protected readonly TcpListener Listener;

        public NodeInstance()
        {
            RootDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            _loadConfig();
            Listener =new TcpListener(IPAddress.Parse(Config.IpAddress),Port);
            _cacheNodes = new CacheData<NodeInformation[]>(_getNodes,TimeSpan.FromHours(1));
        }

        private CacheData<NodeInformation[]> _cacheNodes;

        private async Task<NodeInformation[]> _getNodes()
        {
            var data = new NetworkWorkData() {Action = "GetNodes"};
            NodeListData result = null;
            try
            {
                result =
                    await
                        SendMessage<NetworkWorkData, NodeListData>(NodeInstanceInformarion.Controller, data,
                            "urn:Rikka.S5.S5Core.Messages#NetworkWorkData", "urn:Rikka.S5.S5Core.Messages#NodeListData");
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.Messaging));
            }
            return result?.Nodes;

        }
        public Task<NodeInformation[]> GetNodes()
        {
            return _cacheNodes.GetData();
        }

        /// <summary>
        /// фабрика для создания контекста роли для взаимодействия узла с ролью
        /// </summary>
        /// <param name="rolename"></param>
        /// <returns></returns>
        public RoleContext RoleContextFactory(string rolename,Guid id)
        {
            var ctx = new RoleContext(NodeInstanceInformarion, GetNodes, this, RootDirectory,
                RootDirectory + "\\" + rolename, RootDirectory + "\\" + rolename+"_userdata", id, new NodeLogger(rolename));
            return ctx;
        }
        #region api
        public async Task<S5RoleMessagingDataResult> SendMessage(S5RoleMessagingData msg)
        {
            var result = await SendMessage<S5RoleMessagingData, S5RoleMessagingDataResult>(msg.ToNode, msg,
                "urn:Rikka.S5.S5Core.Messages#S5RoleMessagingData", "urn:Rikka.S5.S5Core.Messages#S5RoleMessagingDataResult");
            return result;

        }

        public NodeInstanceInformarion GetInstance()
        {
            return NodeInstanceInformarion;
        }

        public string GetProperty(string key)
        {
            var prop= Config.Properties.FirstOrDefault(f => f.Key == key);
            return prop?.Value;
        }

        public void SetProperty(string key,string value)
        {
            var prop= Config.Properties.FirstOrDefault(f => f.Key == key);
            if (prop != null)
                prop.Value = value;
            else
            {
                Config.Properties.Add(new KeyValue(key,value));
            }
            _saveConfig();
        }
        #endregion

        #region role management

        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private NodeInternalMessagingResult _roleManagementHandler(NodeInternalMessaging msg)
        {
            var data = SerializationExt.Deserialize<S5RoleManagementData>(msg.Message);
            switch (data.Action)
            {
                case "Install":
                    if (data.DataType != "urn:Rikka.S5.S5Core.Messages#S5RoleInstallData")
                        throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleAction));
                    var installData = SerializationExt.Deserialize<S5RoleInstallData>(data.Data);
                    return _tryInstallRole(data.Rolename, installData);
                //case "Update":
                //    if (data.DataType != "urn:Rikka.S5.S5Core.Messages#S5RoleInstallData")
                //        throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleAction));
                //    var installData = SerializationExt.Deserialize<S5RoleInstallData>(data.Data);
                //    return _tryUpdateRole(data.Rolename, installData);
                case "Uninstall":
                    return _tryUninstallRole(data.Rolename);
                case "Start":
                    return _tryStartRole(data.Rolename);
                case "Stop":
                    return _tryStopRole(data.Rolename);
                case "Status":
                    return _statusRole(data.Rolename);
                default:
                    throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleAction));
            }
        }

        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// попытка установки роли на текущем узле
        /// </summary>
        /// <param name="rolename"></param>
        /// <param name="installData"></param>
        /// <returns></returns>
        public NodeInternalMessagingResult _tryInstallRole(string rolename, S5RoleInstallData installData)
        {
            if(Config.Roles.Any(a=>a.Role==rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleInstallationAlready));
            var userDatamemory = new MemoryStream(installData.ZipUserData);
            var execDatamemory = new MemoryStream(installData.ZipExecData);
            var execzip = new ZipArchive(execDatamemory, ZipArchiveMode.Read);
            var userczip = new ZipArchive(userDatamemory, ZipArchiveMode.Read);
            var roleDirectory = RootDirectory + "\\" + rolename;
            var roleuserDirectory = RootDirectory + "\\" + rolename+"_userdata";
            Directory.CreateDirectory(roleDirectory);
            Directory.CreateDirectory(roleuserDirectory);

            foreach (var entry in execzip.Entries.OrderBy(o => o.FullName))
            {
                var file = roleDirectory +"\\"+ entry.FullName;
                using (var stream = File.OpenWrite(file))
                {
                    entry.Open().CopyTo(stream);
                }
            }
            foreach (var entry in userczip.Entries.OrderBy(o => o.FullName))
            {
                var file = roleuserDirectory + "\\" + entry.FullName;
                using (var stream = File.OpenWrite(file))
                {
                    entry.Open().CopyTo(stream);
                }
            }
            try
            {
                _loadRole(rolename,roleuserDirectory, installData.LoadAssemblies, installData.RoleAssembly, installData.RoleType);
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleInstallation));
            }
            var id = Guid.NewGuid();
            Config.Roles.Add(new RoleData(rolename,id,installData.LoadAssemblies,installData.RoleAssembly,installData.RoleType));
            _saveConfig();
            try
            {
                var roleInstance = Roles.First(f => f.RoleName == rolename);
                roleInstance.Install(RoleContextFactory(rolename,id));
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleInstallation));
            }
            return NodeInternalMessagingResult.FromBool(true);

        }

        /// <summary>
        /// Загрузка происходит в другой AppDomain отдельно для каждой роли
        /// это позволит легко выгружать роль из памяти при удалении/обновлении
        /// не затрагивая остальные роли
        /// </summary>
        /// <param name="rolename"></param>
        /// <param name="dir"></param>
        /// <param name="assemblies"></param>
        /// <param name="mainAssembly"></param>
        /// <param name="type"></param>
        void _loadRole(string rolename,string dir,string[] assemblies,string mainAssembly,string type)
        {
            if(Roles.Any(a=>a.RoleName== rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleLoad));
            var found = false;
            var domainSetup = new AppDomainSetup();
            domainSetup.ApplicationBase = dir;
            var roleDomain = AppDomain.CreateDomain(rolename + "Domain",AppDomain.CurrentDomain.Evidence,domainSetup);
            var roleProxyType = typeof (RefProxy);
            var roleProxy =
                (RefProxy) roleDomain.CreateInstanceAndUnwrap(roleProxyType.Assembly.FullName, roleProxyType.FullName);
            foreach (var file in assemblies)
            {
                var asm = roleProxy.GetAssembly(dir  + "\\" + file);
                if (file == mainAssembly)
                {
                    found = true;
                    var roleType = asm.GetType(type);
                    var roleInstance = Activator.CreateInstance(roleType) as I5Role;
                    if(roleInstance==null || roleInstance.RoleStatus!=S5RoleStatus.Ready)
                        throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleLoad));
                    Roles.Add(roleInstance);
                    NodeInstanceInformarion.Roles.Add(rolename,roleDomain);
                }
            }
            if(!found)
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleLoadMainAssembly));
        }

        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// попытка удаления роли на текущем узле
        /// </summary>
        /// <param name="rolename"></param>
        /// <returns></returns>
        public NodeInternalMessagingResult _tryUninstallRole(string rolename)
        {
            if (Roles.All(a => a.RoleName != rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.UnknownRole));
            var role = Roles.First(f => f.RoleName == rolename);
            if (role.RoleStatus == S5RoleStatus.Started)
                _tryStartRole(rolename);
            var id = Config.Roles.First(f => f.Role == rolename).Id;
            try
            {
                role.Uninstall(RoleContextFactory(rolename,id));
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleUninstall));
            }
            var roleDomain = NodeInstanceInformarion.Roles[rolename];
            AppDomain.Unload(roleDomain);

            var roleDirectory = RootDirectory + "\\" + rolename;
            var roleuserDirectory = RootDirectory + "\\" + rolename + "_userdata";
            Directory.Delete(roleDirectory);
            Directory.Delete(roleuserDirectory);
            return NodeInternalMessagingResult.FromBool(true);
        }

        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// попытка запуска роли на текущем узле
        /// </summary>
        /// <param name="rolename"></param>
        /// <returns></returns>
        public NodeInternalMessagingResult _tryStartRole(string rolename)
        {
            if (Roles.All(a => a.RoleName != rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.UnknownRole));
            var role = Roles.First(f => f.RoleName == rolename);
            var id = Config.Roles.First(f => f.Role == rolename).Id;
            if (role.RoleStatus != S5RoleStatus.Ready)
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleNotReady));
            try
            {
                role.Start(RoleContextFactory(rolename, id));
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleStart));
            }
            return NodeInternalMessagingResult.FromBool(true);
        }
        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// попытка остановки роли на текущем узле
        /// </summary>
        /// <param name="rolename"></param>
        /// <returns></returns>
        public NodeInternalMessagingResult _tryStopRole(string rolename)
        {
            if (Roles.All(a => a.RoleName != rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.UnknownRole));
            var role = Roles.First(f => f.RoleName == rolename);
            if (role.RoleStatus != S5RoleStatus.Started)
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleNotStarted));
            try
            {
                role.Stop();
            }
            catch (NodeInstanceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleStop));
            }
            return NodeInternalMessagingResult.FromBool(true);
        }

        /// <summary>
        /// метод обработки внутренних сообщений сети для управления ролями, отправляется контролером
        /// получение статуса(ов) роли(ей) на текущем узле
        /// </summary>
        /// <param name="rolename"></param>
        /// <returns></returns>
        public NodeInternalMessagingResult _statusRole(string rolename)
        {
            if (Roles.All(a => a.RoleName != rolename))
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.UnknownRole));
            if (rolename != null)
            {
                var role = Roles.First(f => f.RoleName == rolename);
                return
                    NodeInternalMessagingResult.FromData(new S5RoleStatuses()
                    {
                        Statuses = new[] {new KeyValuePair<string, S5RoleStatus>(rolename, role.RoleStatus)}
                    }, "urn:Rikka.S5.S5Core.Messages#S5RoleStatuses");
            }
            else
            {
                return NodeInternalMessagingResult.FromData(new S5RoleStatuses() { Statuses = Roles.Select(s => new KeyValuePair<string, S5RoleStatus>(s.RoleName, s.RoleStatus)).ToArray() }, "urn:Rikka.S5.S5Core.Messages#S5RoleStatuses");
            }
        }
        #endregion

        #region role messaging
        /// <summary>
        /// метод обработки внутренних сообщений сети который управляет сообщениями ролей
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private NodeInternalMessagingResult _s5RoleMessagingHandler(NodeInternalMessaging msg)
        {
            var inner = SerializationExt.Deserialize<S5RoleMessagingData>(msg.Message);
            if(inner.ToNode!=NodeInstanceInformarion.NodeId)
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.MessagingRouting));
            var role = Roles.FirstOrDefault(f => f.RoleName == inner.ToRole);
            if(role==null)
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.RoleNotFound));
            var task = role.Message(inner);
            task.Wait();
            return NodeInternalMessagingResult.FromData(task.Result, "urn:Rikka.S5.S5Core.Messages#S5RoleMessagingDataResult");

        }
        #endregion

        #region controller message routing

        /// <summary>
        /// метод переправки внутренних сообщений контролером сети предназначенному получателю
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private NodeInternalMessagingResult _controllerMessageRoutingHandler(NodeInternalMessaging msg)
        {
            var node = Controller.Nodes.FirstOrDefault(f => f.Id == msg.ToNode);
            if (node != null)
            {
                var task = SendMessage(msg);
                task.Wait();
                return task.Result;
            }
            else
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.MessagingRouting));
            }
        }
        #endregion

        
        public readonly string RootDirectory;
        
        public const string ConfigFile = "config.xml";

        private void _loadConfig()
        {
            using (var stream = File.OpenRead(RootDirectory + "\\" + ConfigFile))
            {
                Config= SerializationExt.Deserialize<NodeConfing>(stream);                
            }
        }

        private void _saveConfig()
        {
            var file = RootDirectory + "\\" + ConfigFile;
            if(File.Exists(file))
                File.Delete(file);
            var xml = SerializationExt.Serialize(Config);
            File.WriteAllText(file,xml);
        }
        /// <summary>
        /// метод записи внутренних сообщений сети в поток
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        private void _writeMessage(Stream stream, NodeInternalMessaging data)
        {
            var ser = new DataContractSerializer(typeof(NodeInternalMessaging));
            var msg = Message.CreateMessage(MessageVersion.Soap12, "", data, ser);
            MessageEncodingBindingElement mb = new BinaryMessageEncodingBindingElement();
            mb.MessageVersion = MessageVersion.Soap12;
            var enc = mb.CreateMessageEncoderFactory();
            var memory = new MemoryStream();
            enc.Encoder.WriteMessage(msg, memory);
            var writer = new StreamWriter(stream);
            int length;
            var buff =  memory.GetBuffer();
            length = buff.Length;
            writer.Write(length);
            stream.Write(buff,0,length);
        }

        /// <summary>
        /// метод записи ответа на внутреннее сообщение сети в поток
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="result"></param>
        private void _writeMessageResult(Stream stream, NodeInternalMessagingResult result)
        {
            var ser = new DataContractSerializer(typeof(NodeInternalMessagingResult));
            var msg = Message.CreateMessage(MessageVersion.Soap12, "", result, ser);
            MessageEncodingBindingElement mb = new BinaryMessageEncodingBindingElement();
            mb.MessageVersion = MessageVersion.Soap12;
            var enc = mb.CreateMessageEncoderFactory();
            enc.Encoder.WriteMessage(msg, stream);
        }
        /// <summary>
        /// метод записи ответа на внутреннее сообщение сети в поток
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <param name="obj"></param>
        /// <param name="resultType"></param>
        private void _writeMessageResult<T>(Stream stream, T obj, string resultType)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = SerializationExt.Serialize(obj),
                ResultType = resultType
            };
            _writeMessageResult(stream, result);
        }

        /// <summary>
        /// считывание внутреннего сообщения сети из потока
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private byte[] _readStreamData(Stream stream)
        {
            int length;
            const short step = 1024;
            var reader = new BinaryReader(stream);
            length = reader.ReadInt32();

            var buff = new byte[length];

            if (length < step)
            {
                stream.Read(buff, 0, length);
            }
            else
            {
                int pos;
                for (pos = 0; pos < length - step; pos += step)
                {
                    stream.Read(buff, pos, step);
                }
                stream.Read(buff, pos, length - pos);
            }
            return buff;
        }

        /// <summary>
        /// считывание внутреннего сообщения сети из потока
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private NodeInternalMessaging _readMessageFromStream(Stream stream)
        {

            var buff = _readStreamData(stream);
            var memory = new MemoryStream(buff);
            NodeInternalMessaging data = null;
            
            MessageEncodingBindingElement mb = new BinaryMessageEncodingBindingElement();
            mb.MessageVersion = MessageVersion.Soap12;

            var enc = mb.CreateMessageEncoderFactory();
            var msg = enc.Encoder.ReadMessage(memory,int.MaxValue);
            var ser = new DataContractSerializer(typeof(NodeInternalMessaging));
            data = msg.GetBody<NodeInternalMessaging>(ser);
            
            return data;
        }

        /// <summary>
        /// метод обработки нового подключения для приема внутреннего сообщения сети
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private delegate NodeInternalMessagingResult MessageHandler(NodeInternalMessaging msg);
        public Dictionary<string, Func<NodeInternalMessaging, NodeInternalMessagingResult>> MessageHandlers;
        private void _readMessage(TcpClient client)
        {
            var stream = client.GetStream();
            var data = _readMessageFromStream(stream);
            if (data == null)
            {
                var error = NodeInstanceException.GerError(NodeInstanceError.MessagingBody);
                _writeMessageResult(stream,error, "urn:Rikka.S5.S5Core.Messages#ErrorInformation");
                throw new NodeInstanceException(error);
            }
            NodeInternalMessagingResult result = null;
            if (data.ToNode != NodeInstanceInformarion.NodeId &&
                NodeInstanceInformarion.NodeId == NodeInstanceInformarion.Controller)
            {
                result = _controllerMessageRoutingHandler(data);

            } else  if (MessageHandlers.ContainsKey(data.MessageType))
            {
                try
                {
                    result = MessageHandlers[data.MessageType].Invoke(data);
                }
                catch (NodeInstanceException e)
                {
                    var error = new ErrorInformation(e.Message, e.ErrorCode);
                    _writeMessageResult(stream, error, "urn:Rikka.S5.S5Core.Messages#ErrorInformation");
                    throw new NodeInstanceException(error);
                }
                catch (Exception e)
                {
                    var error = NodeInstanceException.GerError(NodeInstanceError.Messaging);
                    _writeMessageResult(stream, error, "urn:Rikka.S5.S5Core.Messages#ErrorInformation");
                    throw new NodeInstanceException(error);
                }
            }
            else
            {
                var error = NodeInstanceException.GerError(NodeInstanceError.MessagingUnknownType);
                _writeMessageResult(stream, error, "urn:Rikka.S5.S5Core.Messages#ErrorInformation");
                throw new NodeInstanceException(error);
            }
            _writeMessageResult(stream,result);
            
        }

        /// <summary>
        /// отправка внутреннего сообщения сети текущим узлом
        /// </summary>
        /// <typeparam name="TSend"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="to"></param>
        /// <param name="obj"></param>
        /// <param name="messageType"></param>
        /// <returns></returns>
        protected async Task<TResult> SendMessage<TSend,TResult>(Guid to,TSend obj,string messageType,string resultType)
        {
            var value = SerializationExt.Serialize(obj);
            var msg = new NodeInternalMessaging(this.NodeInstanceInformarion.NodeId,to,value,messageType);
            var result = await SendMessage(msg);
            if (result != null && result.ResultType == resultType)
            {
                var resultData = SerializationExt.Deserialize<TResult>(result.Result);
                return resultData;
            } else if (result != null && result.ResultType == "urn:Rikka.S5.S5Core.Messages#ErrorInformation")
            {
                var error = SerializationExt.Deserialize<ErrorInformation>(result.Result);
                throw new NodeInstanceException(error);
            }
            else
            {
                throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.Messaging));
            }
        }
        /// <summary>
        /// отправка внутреннего сообщения сети текущим узлом
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected async Task<NodeInternalMessagingResult> SendMessage(NodeInternalMessaging msg)
        {
            //TODO: перейти с посредничества контролера на прямую отправку
            var address = this.NodeInstanceInformarion.ControllerAddress;
            if (NodeInstanceInformarion.NodeId == NodeInstanceInformarion.Controller)
            {
                var node = Controller.Nodes.FirstOrDefault(f => f.Id == msg.ToNode);
                if (node != null)
                    address = node.Address;
                else
                {
                    throw new NodeInstanceException(NodeInstanceException.GerError(NodeInstanceError.MessagingRouting));
                }
            }
            var tcp = new TcpClient();
            tcp.Connect(address, Port);
            var stream = tcp.GetStream();
            _writeMessage(stream,msg);

            var buff = _readStreamData(stream);
            var memory = new MemoryStream(buff);
            NodeInternalMessagingResult result = null;
            MessageEncodingBindingElement mb = new BinaryMessageEncodingBindingElement();
            mb.MessageVersion = MessageVersion.Soap12;
            var enc = mb.CreateMessageEncoderFactory();
            var resultMsg = enc.Encoder.ReadMessage(memory, int.MaxValue);
            var ser = new DataContractSerializer(typeof(NodeInternalMessagingResult));
            result = resultMsg.GetBody<NodeInternalMessagingResult>(ser);
            return result;
        }

        /// <summary>
        /// метод обработки нового подключения для приема внутреннего сообщения сети
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        protected Task ReadMessage(TcpClient client)
        {
            return Task.Run(() => _readMessage(client));
        }

        private void _startListen()
        {
            Listener.Start();
            var list = new List<Task>();
            while (true)
            {
                #region отчистка завершенных задач
                var remove = new List<int>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].IsCompleted || list[i].IsFaulted || list[i].IsCanceled)
                    {
                        remove.Add(i);
                    }
                }
                for (int i = 0; i < remove.Count; i++)
                {
                    list.RemoveAt(i);
                }
                #endregion
                var client = Listener.AcceptTcpClient();
                list.Add(ReadMessage(client));
            }
        }


        protected Task StartListen()
        {
            return Task.Run(() => _startListen());
        }


        public void Start()
        {
            MessageHandlers = new Dictionary<string, Func<NodeInternalMessaging, NodeInternalMessagingResult>>();

            MessageHandlers.Add("urn:Rikka.S5.S5Core.Messages#S5RoleMessagingData", _s5RoleMessagingHandler);
            MessageHandlers.Add("urn:Rikka.S5.S5Core.Messages#S5RoleManagementData", _roleManagementHandler);
            //TODO: проверить роли
            foreach (var role in Roles)
            {
                if (role.RoleStatus == S5RoleStatus.Ready)
                {
                    var id = Config.Roles.Single(s => s.Role == role.RoleName).Id;
                    role.Start(RoleContextFactory(role.RoleName,id));
                }
            }
            StartListen();
            if (NodeInstanceInformarion.NodeId != NodeInstanceInformarion.Controller)
                _registerInstance().Wait();
        }

        public void Stop()
        {
            foreach (var role in Roles)
            {
                if (role.RoleStatus == S5RoleStatus.Started)
                {
                    role.Stop();
                }
            }
            Listener.Stop();
        }

        private async Task _registerInstance()
        {
            var register  = new RegisterCredentials();
            var result = await SendMessage<RegisterCredentials,Guid>(NodeInstanceInformarion.Controller, register, "urn:Rikka.S5.S5Core.Messages#RegisterCredentials", DataTypes.UrnUuid);
        }
    }

    /// <summary>
    /// класс для определения контекста для взаимодействия роли с текущим узлом
    /// </summary>
    public class RoleContext : IS5RoleContext
    {
        private readonly Func<Task<NodeInformation[]>> _getNodes;
        internal RoleContext(NodeInstanceInformarion nodeInstanceInformarion,Func<Task<NodeInformation[]>> getNodes, INodeApi api, string rootDirectory, string roleDirectory, string roleUserDirectory, Guid roleId, INodeLogger logger)
        {
            NodeInstanceInformarion = nodeInstanceInformarion;
            Api = api;
            RootDirectory = rootDirectory;
            RoleDirectory = roleDirectory;
            RoleUserDirectory = roleUserDirectory;
            RoleId = roleId;
            Logger = logger;
            _getNodes = getNodes;
        }

        public NodeInstanceInformarion NodeInstanceInformarion { get; }

        public Task<NodeInformation[]> GetNodesInformation()
        {
            return _getNodes.Invoke();
        }

        public Guid RoleId { get; }

        public INodeApi Api { get; }
        public INodeLogger Logger { get; }
        public string RootDirectory { get; }
        public string RoleDirectory { get; }
        public string RoleUserDirectory { get; }
    }

    public class NodeLogger : INodeLogger
    {
        private readonly string _logger;

        public NodeLogger(string logger)
        {
            _logger = logger;
        }

        public void Log(string level, string subject, string message)
        {
            System.Diagnostics.Debug.Write($"[{level}] {_logger}: {subject}. {message}");
        }

        public void Info(string subject, string message)
        {
            Log("INFO", subject, message);
        }

        public void Warn(string subject, string message)
        {
            Log("WARN", subject, message);
        }

        public void Error(string subject, string message)
        {
            Log("ERROR", subject, message);
        }
    }

    public class NodeConfing
    {
        /// <summary>
        /// используемый адрес для прослушки
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// общий клч сети
        /// </summary>        
        public string NetworkKey { get; set; }

        /// <summary>
        /// ид текущего узла
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ид контролера сети
        /// </summary>
        public Guid Controller { get; set; }

        /// <summary>
        /// дополнительные параметры ключ-значение
        /// </summary>
        public List<KeyValue> Properties { get; set; }

        /// <summary>
        /// установленные роли
        /// </summary>
        [XmlArrayItem("Role")]
        public List<RoleData> Roles { get; set; }
    }

    public class RoleData
    {
        public RoleData()
        {

        }

        public RoleData(string role,Guid id,string[] assemblies,string mainAssembly,string roleType)
        {
            Role = role;
            Id = id;
            Assemblies = assemblies;
            MainAssembly = mainAssembly;
            RoleType = roleType;
        }
        public string Role { get; set; }
        public Guid Id { get; set; }
        public string[] Assemblies { get; set; }
        public string MainAssembly { get; set; }
        public string RoleType { get; set; }
    }

    public class KeyValue
    {
        public KeyValue()
        {
            
        }

        public KeyValue(string key,string value)
        {
            Key = key;
            Value = value;
        }
        public string Key { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// класс описывает структуру внутреннего сообщения сети
    /// </summary>
    [DataContract]
    public class NodeInternalMessaging
    {
        public NodeInternalMessaging()
        {
            
        }

        public NodeInternalMessaging(Guid from, Guid to, string message, string messageType)
        {
            FromNode = from;
            ToNode = to;
            Message = message;
            MessageType = messageType;
        }
        [DataMember]
        public Guid FromNode { get; set; }
        [DataMember]
        public Guid ToNode { get; set; }

        /// <summary>
        /// содержит Xml или другие данные
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// определяет тип данных для раскодировки
        /// </summary>
        [DataMember]
        public string MessageType { get; set; }
    }

    [DataContract]
    public class NodeInternalMessagingResult
    {
        /// <summary>
        /// содержит Xml или другие данные
        /// </summary>
        [DataMember]
        public string Result { get; set; }

        /// <summary>
        /// определяет тип данных для раскодировки
        /// </summary>
        [DataMember]
        public string ResultType { get; set; }

        public static NodeInternalMessagingResult FromData<T>(T obj, string objType)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = SerializationExt.Serialize<T>(obj),
                ResultType = objType
            };
            return result;
        }
        public static NodeInternalMessagingResult FromBool(bool value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = value ? "true" : "false",
                ResultType = DataTypes.Boolean
            };
            return result;
        }
        public static NodeInternalMessagingResult FromInt(int value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = value.ToString(),
                ResultType = DataTypes.Integer
            };
            return result;
        }
        public static NodeInternalMessagingResult FromFloat(float value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = value.ToString(CultureInfo.InvariantCulture),
                ResultType = DataTypes.Float
            };
            return result;
        }
        public static NodeInternalMessagingResult FromDateTime(DateTime value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = value.ToString("o"),
                ResultType = DataTypes.DateTime
            };
            return result;
        }
        public static NodeInternalMessagingResult FromString(string value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = value,
                ResultType = DataTypes.String
            };
            return result;
        }
        public static NodeInternalMessagingResult FromGuid(Guid value)
        {
            var result = new NodeInternalMessagingResult()
            {
                Result = "urn:uuid:"+value,
                ResultType = DataTypes.UrnUuid
            };
            return result;
        }
    }

    [Serializable]
    public class NodeInstanceException : Exception
    {
        public readonly string ErrorCode;

        public NodeInstanceException(ErrorInformation error) : base(error.Error)
        {
            this.ErrorCode = error.ErrorCode;
        }        


        public NodeInstanceException(string message,string errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }

       
        public static ErrorInformation GerError(NodeInstanceError error)
        {
            return new ErrorInformation("DEV", "DEV");
            switch (error)
            {
                //TODO:поставить
            }
        }
    }

    public enum NodeInstanceError
    {
        Unknown,

        Io,
        IoSecurity,

        Network,
        NetworkStream,
        NetworkStartListen,
        NetworkDns,
        NetworkRefused,
        NetworkConnect,
        NetworkConnectTimeout,

        Messaging,
        MessagingRouting,
        MessagingBody,
        MessagingUnknownType,


        NodeUnknown,
        NodeSecurity,

        RoleUnknown,
        RoleAction,
        RoleExecution,

        RoleInstallationAlready,
        RoleLoadMainAssembly,
        RoleInstallation,
        RoleLoad,
        RoleUninstall,
        UnknownRole,
        RoleNotReady,
        RoleStart,
        RoleNotStarted,
        RoleStop,
        RoleNotFound
    }

    public class CacheData<T>
    {
        private readonly Func<Task<T>> _getData;

        private T _data;

        private readonly TimeSpan _span;

        private DateTime? _stamp;

        public CacheData(Func<Task<T>> getDataFunc, TimeSpan span)
        {
            _getData = getDataFunc;
            _span = span;
        }

        public Task<T> GetData()
        {
            if (!_stamp.HasValue || _stamp + _span < DateTime.Now)
            {
                var task = _getData();
                task.Wait();
                _data = task.Result;
                _stamp = DateTime.Now;
            }
            return Task.FromResult(_data);
        }
    }

    public class RefProxy : MarshalByRefObject
    {
        public Assembly GetAssembly(string path)
        {
            try
            {
                return Assembly.LoadFile(path);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

}
