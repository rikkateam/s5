﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace test.testSrv1
{
    public class testSrv : ItestSrv
    {
        public int Message1(int param1)
        {
            return param1 * param1;
        }

        public string Message2(int param1, string param2)
        {
            return param1 + param2;
        }

        public static List<ComplexInnerData> Items = new List<ComplexInnerData>();

        public ComplexDataResult Message3(ComplexData data)
        {
            Items.Add(data.Data);
            return new ComplexDataResult()
            {
                Str = data.Str1+data.Int1,
                DataItems = Items.ToArray()
            };
        }
    }
}
