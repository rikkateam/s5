﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace test.testSrv1
{
    [ServiceContract]
    public interface ItestSrv
    {
        [OperationContract]

        int Message1(int param1);

        [OperationContract]
        string Message2(int param1, string param2);
        [OperationContract]
        ComplexDataResult Message3(ComplexData data);
    }

    [DataContract]
    public class ComplexData
    {
        [DataMember]
        public string Str1 { get; set; }

        [DataMember]
        public int Int1 { get; set; }

        [DataMember]
        public ComplexInnerData Data { get; set; }        
    }

    [DataContract]
    public class ComplexDataResult
    {
        [DataMember]
        public string Str { get; set; }
        [DataMember]
        public ComplexInnerData[] DataItems { get; set; }
    }

    [DataContract]
    public class ComplexInnerData
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Value { get; set; }
    }
}
