﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rikka.S5.S5Core;
using Rikka.S5.Wf.Core.Passport;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var points = new WfFlowContainer();
            points.Points = new object[]
            {
                new WffStartPoint()
                {
                    Name="start",
                    Key="startPoint",
                    StateName = "startPointState",
                },
                new WffFormPoint()
                {
                    Name="form1",
                    Key="form1Point",
                    StateName = "form1PointState",
                    FormKey = "form1"
                },
                new WffCondPoint()
                {
                    Name="cond1",
                    Key="cond1Point",
                    StateName = "cond1PointState",
                    Then = new WfFlowContainer()
                    {
                        Points = new object[]{
                            new WffStopPoint()
                            {
                                Name="stop1",
                                Key="stop1Point",
                                StateName = "stop1PointState",
                            }
                        },
                        PointsChoice = new [] {WffContainerPoinsChoiceType.StopPoint, }
                    },
                    Else = new WfFlowContainer()
                    {
                        Points = new object[]{
                            
                            new WffComplexPoint()
                            {
                                Name="complex1",
                                Key="complex1Point",
                                StateName = "complex1PointState",
                            }, 
                            new WffStopPoint()
                            {
                                Name="stop2",
                                Key="stop2Point",
                                StateName = "stop2PointState",
                            }
                        },
                        PointsChoice = new [] {WffContainerPoinsChoiceType.ComplexPoint, WffContainerPoinsChoiceType.StopPoint, }
                    }
                }
            };
            var resources = new WfResources()
            {
                Resources = new object[]
                {
                    new WfFormResource()
                    {
                        Name = "form1",
                        Key = "form1",
                        Elements = new WfFormElement[]
                        {
                            new WfFormElement()
                            {
                                Name = "field1",
                                Key = "field1",
                                Variable = "param1",
                                ValueType = WfDataType.Int,
                                Many = false,
                                Order = 1,
                                EditorType = WfFormElementEditorType.Field,
                            },
                            new WfFormElement()
                            {
                                Name = "field2",
                                Key = "field2",
                                Variable = "param2",
                                ValueType = WfDataType.Int,
                                Many = false,
                                Order = 2,
                                EditorType = WfFormElementEditorType.Picker,
                                Parameters = new ComplexElementParameters()
                                {
                                    Items = new object[]
                                    {
                                        new WffePickerData() {SelectKey = "directory1"}
                                    },
                                    ItemsChoice = new [] { CepItemsChoiceType .PickerData}
                                }
                            }
                        }
                    },
                    new WfDirectoryResource()
                    {
                        Name = "directory1",
                        Key="directory1",
                        Container = new WfDirectoryContainer()
                        {
                            KeyType = WfDataType.Text,
                            KeyName = "name",
                            ValueType = WfDataType.Int,
                            ValueName = "value"
                        }
                    },
                    new WfSrvSoapAction()
                    {
                        Name = "soap1",
                        Key = "soap1",
                        ActionEndPoint = "http://localhost:55074/testSrv.svc",
                        PortTypeName = "message1",
                        OperationName = "message1",
                        WsdlSchemaNamespace = "urn:test.testSrv1",
                        WsdlSchemaLocation = "testSrv1.xsd",
                        BindingInput = new [] {new WfsaBinding() { Binding = "param1", ParameterName = "param1" } },
                        BindingOutput= new [] {new WfsaBinding() { Binding = "param2", ParameterName = "param2" } },
                        SendProcessor = new WfsaProcessor() {Command = ";;;"},
                        ReceiveProcessor = new WfsaProcessor() {Command = ";;;"},
                    }
                },
                ResourcesChoice = new []{ WfrResourcesChoiceType.FormResource,WfrResourcesChoiceType.DirectoryResource, WfrResourcesChoiceType.SrvSoapAction }
            };
            points.PointsChoice = new [] {WffContainerPoinsChoiceType.StartPoint, WffContainerPoinsChoiceType.FormPoint, WffContainerPoinsChoiceType.CondPoint};
            var workflow = new WfFlow()
            {
                Glolbals = new[]
                {
                    new WfVariable() {Name = "global1", DefaultValueExpression = "11", ValueType = WfDataType.Int},
                    new WfVariable() {Name = "global2", DefaultValueExpression = "22", ValueType = WfDataType.Int},
                    new WfVariable() {Name = "global3", DefaultValueExpression = "asd", ValueType = WfDataType.Text},
                },
                Variables = new[]
                {
                    new WfVariable() {Name = "param1", DefaultValueExpression = "1", ValueType = WfDataType.Int},
                    new WfVariable() {Name = "param2", DefaultValueExpression = "2", ValueType = WfDataType.Int},
                    new WfVariable() {Name = "param3", DefaultValueExpression = "qwe", ValueType = WfDataType.Text},
                },
                Points = points,
                Resources = resources
            };
            var passport = new WfPassport()
            {
                Key = "key",
                Name = "name",
                Description = "description",
                Doc = "doc",
                Version = "1.0.0",
                Workflow = workflow
            };
            var ser = new XmlSerializer(typeof(WfPassport));
            using (var stream = System.IO.File.OpenWrite("passport1.xml"))
            {
                ser.Serialize(stream,passport);
            }
        }
    }
}
