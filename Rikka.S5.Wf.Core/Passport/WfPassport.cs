﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core;

namespace Rikka.S5.Wf.Core.Passport
{
    /// <summary>
    /// класс определяет структуру XML для хранения всей необходимой информации о бизнес процессе
    /// каждый такой паспорт хранит описание одного бизнес процесса
    /// </summary>
    [XmlRoot("Passport",Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfPassport
    {
        [XmlAttribute]
        public string Key { get; set; }

        [XmlElement(IsNullable = false)]        
        public string Name { get; set; }

        /// <summary>
        /// версия паспорта, каждая версия должна храниться для продолжения работы старых бизнес процессов
        /// новые бизнес процессы будут работать с новейшей версией
        /// </summary>
        [XmlAttribute]
        public string Version { get; set; }

        public string Description { get; set; }

        public string Doc { get; set; }

        public WfFlow Workflow { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. описание бизнес процесса
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFlow
    {
        /// <summary>
        /// переменные создаваемые на каждый экземпляр
        /// </summary>
        [XmlArrayItem("Item")]
        public WfVariable[] Variables { get; set; }
        /// <summary>
        /// глобальные переменные на весь бизнес процесс
        /// </summary>
        [XmlArrayItem("Item")]
        public WfVariable[] Glolbals { get; set; }
        /// <summary>
        /// описание задачи "процесса" бизнес процесса
        /// </summary>
        public WfFlowContainer Points { get; set; }
        /// <summary>
        /// ресурсы испольуемые в бизнес процессе
        /// </summary>
        public WfResources Resources { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. описание переменных
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfVariable
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public WfDataType ValueType { get; set; }
        /// <summary>
        /// выражение на скриптовом языке для значения по умолчанию
        /// </summary>
        public string DefaultValueExpression { get; set; }
    }



    /// <summary>
    /// класс определяет структуру XML. описывает множество задач различного типа
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFlowContainer
    {
        [XmlChoiceIdentifier("PointsChoice")]

        [XmlElement("Point", typeof(WffPoint))]
        [XmlElement("Sequence", typeof(WfFlowSequence))]
        [XmlElement("Parallel", typeof(WfFlowParallel))]

        [XmlElement("ComplexPoint", typeof(WffComplexPoint))]
        [XmlElement("StartPoint", typeof(WffStartPoint))]
        [XmlElement("StopPoint", typeof(WffStopPoint))]
        [XmlElement("CondPoint", typeof(WffCondPoint))]
        [XmlElement("SrvActionPoint", typeof(WffSrvActionPoint))]
        [XmlElement("FormPoint", typeof(WffFormPoint))]
        [XmlElement("EventWaitPoint", typeof(WffEventWaitPoint))]
        [XmlElement("EventEmitPoint", typeof(WffEventEmitPoint))]

        public object[] Points { get; set; }

        [XmlIgnore]
        public WffContainerPoinsChoiceType[] PointsChoice;
    }

    public enum WffContainerPoinsChoiceType
    {
        None,Point,Sequence,Parallel,ComplexPoint,StartPoint,StopPoint,CondPoint,SrvActionPoint,FormPoint,EventWaitPoint,EventEmitPoint
    }

    #region wf-points
    /// <summary>
    /// класс определяет структуру XML. простая задача содержащая лишь название
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffPoint
    {
        public string Name { get; set; }

        [XmlAttribute]
        public string Key { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public string Doc { get; set; }
    }
    /// <summary>
    /// класс определяет структуру XML. последовательность задач в которой может быть любые другие типы задач
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFlowSequence
    {
        public WfFlowContainer Points { get; set; }
    }
    /// <summary>
    /// класс определяет структуру XML. множество задач которые запускаются паралельно
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFlowParallel
    {
        public WfFlowContainer Points { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. сложная задача, используется другими задачами
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffComplexPoint: WffPoint
    {
        public FlowComplexData Parameters { get; set; }
    }


    /// <summary>
    /// класс определяет структуру XML. сложная задача обращения к внешней службе
    /// за ее работу отвечают Rikka.S5.Wf.WsCore 
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffSrvActionPoint : WffComplexPoint
    {
        public string ActionKey { get; set; }

        public bool Autoretry { get; set; }

        public int AutoretryCount { get; set; }

        public int AutoretryAfter { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. сложная задача ожидающая событие
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffEventWaitPoint : WffComplexPoint
    {
        public string Event { get; set; }

        public string ValidateEventExpression { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. Сложная задача запускающая событие
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffEventEmitPoint : WffComplexPoint
    {
        public string Event { get; set; }

        public string ValueXpression { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. стартовая точка
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffStartPoint : WffPoint
    {

    }

    /// <summary>
    /// класс определяет структуру XML. задача определяет оператор ветвления на условии
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffCondPoint : WffPoint
    {
        public string ConditionExpression { get; set; }

        public WfFlowContainer Then { get; set; }
        public WfFlowContainer Else { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. задача завершает экземпляр бизнес процесса
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffStopPoint : WffPoint
    {

    }

    /// <summary>
    /// класс определяет структуру XML. сложная задача взаимодействующая с пользователем
    /// на основе формы
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffFormPoint : WffComplexPoint
    {
        public string FormKey { get; set; }
    }
    #endregion

    #region wf-points data
    /// <summary>
    /// класс определяет структуру XML. контейнер для раззличных параметров сложных задач
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class FlowComplexData
    {
        [XmlChoiceIdentifier("ItemChoice")]
        [XmlElement("Variable", typeof(WfVariable))]        
        public object[] Item { get; set; }

        [XmlIgnore]
        public FcdItemChoiceType[] ItemChoice { get; set; }
    }
    
    public enum FcdItemChoiceType
    {
        None, Variable
    }

    #endregion

    #region wf-form
    /// <summary>
    /// класс определяет структуру XML. элемент формы
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFormElement
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public bool Many { get; set; }
        public string Variable { get; set; }
        public WfDataType ValueType { get; set; }
        public WfFormElementEditorType EditorType {get; set; }
        public ComplexElementParameters Parameters { get; set; }
        public string Description { get; set; }
        public string Doc { get; set; }
        public int Order { get; set; }
    }

    public enum WfFormElementEditorType
    {
        Field,
        ComboBox,
        Picker
    }

    #endregion

    #region wf-form parameters
    /// <summary>
    /// класс определяет структуру XML. параметры элемента формы
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class ComplexElementParameters
    {
        [XmlChoiceIdentifier("ItemsChoice")]
        [XmlElement("RequiredValidator", typeof(WffeRequiredValidator))]
        [XmlElement("LengthValidator", typeof(WffeLengthValidator))]
        [XmlElement("RegexValidator", typeof(WffeRegexValidator))]
        [XmlElement("RangeValidator", typeof(WffeRangeValidator))]
        [XmlElement("DateValidator", typeof(WffeDateValidator))]
        [XmlElement("PickerData", typeof(WffePickerData))]
        public object[] Items { get; set; }

        [XmlIgnore]
        public CepItemsChoiceType[] ItemsChoice { get; set; }
    }

    public enum CepItemsChoiceType
    {
        None,RequiredValidator, LengthValidator, RegexValidator, RangeValidator, DateValidator, PickerData
    }
    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для валидации обязательности поля
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffeRequiredValidator
    {
        public bool Required { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для валидации длины значения
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffeLengthValidator
    {
        public int Length { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для валидации на основе регулярного выражения
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffeRegexValidator
    {
        public string Regex { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для валидации диапазона значений
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffeRangeValidator
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для валидации диапазона даты
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffeDateValidator
    {
        public string Min { get; set; }
        public string Max { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. параметр элемента формы для выборки из справочников/реестров и пр.
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WffePickerData
    {
        public string SelectKey { get; set; }

    }
    #endregion

    #region wf-services
    /// <summary>
    /// класс определяет структуру XML. ресурс определяющий запуск обращения к простым службам json/xml
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfSrvBasicAction: IWfSrvAction
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// xml или json
        /// </summary>
        public HttpActionDataType DataType { get; set; }

        public string ActionUrl { get; set; }
        public string HttpMethod { get; set; }

        public WfsaBinding[] BindingInput { get; set; }
        public WfsaBinding[] BindingOutput { get; set; }

        public WfsaProcessor PreSendProcessor { get; set; }
        public WfsaProcessor SendProcessor { get; set; }
        public WfsaProcessor PreReceiveProcessor { get; set; }
        public WfsaProcessor ReceiveProcessor { get; set; }
    }

    public enum HttpActionDataType
    {
        Xml,
        Json,
    }
    
    public class WfsaProcessor
    {
        public string Command { get; set; }
    }

    [XmlType(IncludeInSchema = false)]
    public interface IWfSrvAction
    {
        string Name { get; set; }
        string Key { get; set; }
        string Description { get; set; }        

        WfsaBinding[] BindingInput { get; set; }
        WfsaBinding[] BindingOutput { get; set; }

        WfsaProcessor PreSendProcessor { get; set; }
        WfsaProcessor SendProcessor { get; set; }
        WfsaProcessor PreReceiveProcessor { get; set; }
        WfsaProcessor ReceiveProcessor { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс определяющий обращение к службам soap
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfSrvSoapAction:IWfSrvAction
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }

        public string WsdlSchemaLocation { get; set; }
        public string WsdlSchemaNamespace { get; set; }
        public string PortTypeName { get; set; }
        public string OperationName { get; set; }

        public string ActionEndPoint { get; set; }

        public SoapVersionType SoapVersion { get; set; }

        public WfsaBinding[] BindingInput { get; set; }
        public WfsaBinding[] BindingOutput { get; set; }

        public WfsaProcessor PreSendProcessor { get; set; }
        public WfsaProcessor SendProcessor { get; set; }
        public WfsaProcessor PreReceiveProcessor { get; set; }
        public WfsaProcessor ReceiveProcessor { get; set; }
    }
    

    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfsaBinding
    {
        public string ParameterName { get; set; }
        /// <summary>
        /// Выражение - обращение к переменной или значение
        /// </summary>
        public string Binding { get; set; }
    }

    #endregion
    #region wf-resources
    /// <summary>
    /// класс определяет структуру XML. контейнер ресурсов
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfResources
    {
        [XmlChoiceIdentifier("ResourcesChoice")]
        [XmlElement("SrvSoapAction", typeof(WfSrvSoapAction))]
        [XmlElement("SrvBasicAction", typeof(WfSrvBasicAction))]
        [XmlElement("DictionaryResource", typeof(WfDictionaryResource))]
        [XmlElement("DirectoryResource", typeof(WfDirectoryResource))]
        [XmlElement("IndexResource", typeof(WfIndexResource))]
        [XmlElement("GeneratorResource", typeof(WfGeneratorResource))]        
        [XmlElement("FormResource", typeof(WfFormResource))]
        [XmlElement("EventResource", typeof(WfEventResource))]
        public object[] Resources { get; set; }        

        [XmlIgnore]
        public WfrResourcesChoiceType[] ResourcesChoice { get; set; }
    }

    public enum WfrResourcesChoiceType
    {
        None,SrvSoapAction, SrvBasicAction, DictionaryResource, DirectoryResource, IndexResource, GeneratorResource, FormResource,EventResource
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс справочника
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfDirectoryResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }

        public WfDirectoryContainer Container { get; set; }
    }


    /// <summary>
    /// класс определяет структуру XML. контейнер ресурса справочника
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfDirectoryContainer
    {
        public string KeyName { get; set; }
        public WfDataType KeyType { get; set; }
        public string ValueName { get; set; }
        public WfDataType ValueType { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс определения строго заданного справочника
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfDictionaryResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }

        public WfDictionaryContainer Container { get; set; }
    }
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfDictionaryContainer
    {
        public WfDictionaryItem[] Items { get; set; }
    }
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfDictionaryItem
    {
        [XmlAttribute]
        public string Key { get; set; }        
        public string Value { get; set; }        
    }
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfIndexResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }

        public int Step { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс генератора
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfGeneratorResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }

        public string Template { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс описывающий форму
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfFormResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }

        [XmlArrayItem("Item")]
        public WfFormElement[] Elements { get; set; }
        public string Doc { get; set; }
    }

    /// <summary>
    /// класс определяет структуру XML. ресурс описания события
    /// </summary>
    [XmlType(Namespace = "urn:Rikka.S5.Wf.Core.Passport")]
    public class WfEventResource
    {
        public string Name { get; set; }
        [XmlAttribute]
        public string Key { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }
        public string Event { get; set; }
    }

    //TODO: wf-resource: реляционные реестры - структура описания реляционной БД
    #endregion

}
