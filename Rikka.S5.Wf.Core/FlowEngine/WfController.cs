﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core;
using Rikka.S5.S5Core.Messages;
using Rikka.S5.Wf.Core.Passport;

namespace Rikka.S5.Wf.Core.FlowEngine
{


    /// <summary>
    /// роль движка workflow
    /// </summary>
    public class WorkflowCoreRole : I5Role
    {
        public string RoleVersion { get; set; }
        public string RoleName => "WorkflowCoreSpRole";
        public void Start(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Starting;
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Started;
        }

        public void Stop()
        {
            RoleStatus = S5RoleStatus.Stoping;
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Stoped;
        }

        public S5RoleStatus RoleStatus { get; private set; }

        public void Install(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Installing;
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Ready;
        }

        public void Uninstall(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Uninstalling;
            throw new NotImplementedException();
        }

        public Task<S5RoleMessagingDataResult> Message(S5RoleMessagingData msg)
        {
            throw new NotImplementedException();
        }
    }
}
