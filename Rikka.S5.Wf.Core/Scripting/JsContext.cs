﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.ClearScript;
using Microsoft.ClearScript.JavaScript;
using Microsoft.ClearScript.V8;

namespace Rikka.S5.Wf.Core.Scripting
{
    /// <summary>
    /// класс управления окружением скриптов
    /// </summary>
    public class JsContext
    {
        public ExpandoObject Locals { get; set; }
        public ExpandoObject Globals { get; set; }
        public ExpandoObject Properties { get; set; }

        private readonly V8ScriptEngine _engine;

        public JsContext(object[] locals,object[] globals,object[] properties)
        {
            _engine = new V8ScriptEngine();
        }

        /// <summary>
        /// проверка на изменение локальных и глобальных переменных, параметров и пр.
        /// и последующее обновление
        /// </summary>
        private void _detectChanges()
        {
            throw new NotImplementedException();
        }
        private void _exec(string script)
        {
            throw new NotImplementedException();
            _detectChanges();
        }
        public void Exec(string script)
        {
            _exec(script);
            throw new NotImplementedException();
        }

        private object _execExpression(string script)
        {
            throw new NotImplementedException();
            _detectChanges();
        }
        public T ExecExpression<T>(string script)
        {
            object result=null;
            try
            {
                result = _execExpression(script);
            }
            catch (Exception e)
            {
                
            }
            if (typeof (bool) == typeof (T))
            {
                return (T)result;
            }
            else if (typeof(string) == typeof(T))
            {
                return (T)result;
            }
            else if (typeof(int) == typeof(T))
            {
                return (T)result;
            }
            else if (typeof(float) == typeof(T))
            {
                return (T)result;
            }
            else if (typeof (DateTime) == typeof (T))
            {
                throw new NotImplementedException();
            }
            else
            {
                throw new TypeLoadException();
            }
        }
    }

    /// <summary>
    /// класс для построения объектов на основе переменных рабочего процесса (локальных, глобальных и пр.)
    /// </summary>
    public class JsVariableBuilder
    {
        public void Add(string name, object value,string valueType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// построение всех переменных в один объект
        /// </summary>
        /// <returns></returns>
        public ExpandoObject GetVariables()
        {
            throw new NotImplementedException();
        }
    }
}
