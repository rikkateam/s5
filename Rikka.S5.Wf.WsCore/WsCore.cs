﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core;
using Rikka.S5.S5Core.Messages;
using Rikka.S5.Wf.WsCoreClient;

namespace Rikka.S5.Wf.WsCore
{
    /// <summary>
    /// класс управляющий созданием и компиляцией клиента сервиса
    /// </summary>
    public class ClientGenerator
    {
        public void Test(string file)
        {
            var name = file.Substring(0, file.LastIndexOf("."));
            ExecCommand($"svcutil {file}");
            System.IO.File.Exists($"{name}.cs");
            ExecCommand($"csc {name}.cs /target:library");
            System.IO.File.Exists($"{name}.dll");
        }

        public void ExecCommand(string command)
        {
            //todo: to-do
        }
    }

    /// <summary>
    /// роль управления веб сервисами
    /// </summary>
    public class WebServiceRole : I5Role
    {
        public string RoleVersion { get; set; }

        public string RoleName => "WebServiceSpRole";

        protected  List<MessageHandler> Handlers;
        public void Start(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Starting;
            Handlers = new List<MessageHandler>
            {
                new MessageHandler(WebServiceRoleClient.CreateWsClientAction,
                    WebServiceRoleClient.CreateWsClientValueType, WebServiceRoleClient.CreateWsClientValueType,
                     CreateWsClient
                    ),
                new MessageHandler(WebServiceRoleClient.CompileWsClientAction,
                    WebServiceRoleClient.CompileWsClientValueType, WebServiceRoleClient.CompileWsClientResultValueType,
                     CompileWsClient
                    ),
                new MessageHandler(WebServiceRoleClient.GetWsClientsAction,
                    null, WebServiceRoleClient.GetWsClientsResultValueType,
                     GetWsClients
                    )
            };
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Started;
        }

        public void Stop()
        {
            RoleStatus = S5RoleStatus.Stoping;
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Stoped;
        }

        public S5RoleStatus RoleStatus { get; private set; }

        public void Install(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Installing;
            throw new NotImplementedException();
            RoleStatus = S5RoleStatus.Ready;
        }

        public void Uninstall(IS5RoleContext instance)
        {
            RoleStatus = S5RoleStatus.Uninstalling;
            throw new NotImplementedException();
        }

        
        public async Task<S5RoleMessagingDataResult> Message(S5RoleMessagingData msg)
        {
            var handler = Handlers.FirstOrDefault(f => f.Action == msg.Action && f.ValueType == msg.ValueType);
            if (handler != null)
            {
                S5RoleMessagingDataResult result =null;
                try
                {
                    result = await handler.Exec(msg);
                }
                catch (Exception e)
                {
                    throw new NotImplementedException();
                }
                return result;
            }
            throw new NotImplementedException();
        }

        private void Prepare<TSend>(S5RoleMessagingData msg,out TSend sendData) where TSend : class
        {
            if (msg.Value != null)
            {
                var sendSer = new XmlSerializer(typeof(TSend));
                using (var stream = new MemoryStream())
                {
                    var buffer = Encoding.UTF8.GetBytes(msg.Value);
                    stream.Write(buffer, 0, buffer.Length);
                    sendData = sendSer.Deserialize(stream) as TSend;
                }
            }
            else
            {
                sendData = null;
            }
        }

        public S5RoleMessagingDataResult GetResult<TResult>(TResult result,string valueType)
        {
            var value = SerializationExt.Serialize<TResult>(result);
            var dataResult = new S5RoleMessagingDataResult() { ValueType = valueType,Value=value};
            return dataResult;

        }

        public Task<WsProjectData> CreateWsClient(CreateWsData data)
        {
            throw new NotImplementedException();

        }
        public async Task<S5RoleMessagingDataResult> CreateWsClient(S5RoleMessagingData msg)
        {
            CreateWsData data = null;
            Prepare<CreateWsData>(msg,out data);
            var result = await CreateWsClient(data);
            var dataResult = GetResult(result, WebServiceRoleClient.CompileWsClientResultValueType);
            return dataResult;
        }

        public Task<WsProjectData> CompileWsClient(WsProject data)
        {
            throw new NotImplementedException();

        }
        public async Task<S5RoleMessagingDataResult> CompileWsClient(S5RoleMessagingData msg)
        {
            WsProject data = null;
            Prepare<WsProject>(msg, out data);
            var result = await CompileWsClient(data);
            var dataResult = GetResult(result, WebServiceRoleClient.CompileWsClientResultValueType);
            return dataResult;
        }

        public async Task<WsClientList> GetWsClients()
        {
            throw new NotImplementedException();

        }
        public async Task<S5RoleMessagingDataResult> GetWsClients(S5RoleMessagingData msg)
        {
            var result = await GetWsClients();
            var dataResult = GetResult(result, WebServiceRoleClient.CompileWsClientResultValueType);
            return dataResult;
        }
    }

    public delegate Task<S5RoleMessagingDataResult> MessageHandlerDelegate(S5RoleMessagingData msg);

    public class MessageHandler
    {
        public string Action { get; set; }
        public string ValueType { get; set; }
        public MessageHandlerDelegate Exec;

        public MessageHandler(string action, string valueType, string resultValueType, MessageHandlerDelegate exec)
        {
            Action = action;
            ValueType = valueType;
            Exec = exec;
        }
    }
}
