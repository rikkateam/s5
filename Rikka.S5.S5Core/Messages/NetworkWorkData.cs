﻿using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    /// <summary>
    /// данные для передачи через внутренние сообщения сети
    /// </summary>
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class NetworkWorkData
    {
        public string Action { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
    }
}