﻿using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    /// <summary>
    /// данные для передачи через внутренние сообщения сети
    /// </summary>
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class ErrorInformation
    {
        public ErrorInformation()
        {

        }
        public ErrorInformation(string error, string errorCode)
        {
            Error = error;
            ErrorCode = errorCode;
        }
        public string Error { get; set; }
        public string ErrorCode { get; set; }
        public string StackTrace { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
    }
}