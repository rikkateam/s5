using System.Collections.Generic;
using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class S5RoleStatuses
    {
        public KeyValuePair<string, S5RoleStatus>[] Statuses { get; set; }
    }
}