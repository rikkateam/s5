﻿using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class S5RoleInstallData
    {
        public string Version { get; set; }
        /// <summary>
        /// zip архив в котором находятся исполняемые файлы
        /// </summary>
        public byte[] ZipExecData { get; set; }

        /// <summary>
        /// zip архив в котором находятся пользовательские файлы -- при клонировании на основе другой роли
        /// </summary>
        public byte[] ZipUserData { get; set; }

        public string[] LoadAssemblies { get; set; }

        public string RoleAssembly { get; set; }

        public string RoleType { get; set; }
    }
}