using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class NodeListData
    {
        public NodeInformation[] Nodes { get; set; }
    }
}