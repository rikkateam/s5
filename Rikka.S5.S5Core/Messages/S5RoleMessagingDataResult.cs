﻿using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    /// <summary>
    /// данные для передачи через внутренние сообщения сети
    /// </summary>
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class S5RoleMessagingDataResult
    {
        public string ValueType { get; set; }
        public string Value { get; set; }
    }
}