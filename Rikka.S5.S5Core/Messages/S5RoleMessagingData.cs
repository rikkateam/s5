﻿using System;
using System.Xml.Serialization;

namespace Rikka.S5.S5Core.Messages
{
    /// <summary>
    /// данные для передачи через внутренние сообщения сети
    /// </summary>
    [XmlRoot(Namespace = "urn:Rikka.S5.S5Core.Messages")]
    public class S5RoleMessagingData
    {
        public S5RoleMessagingData()
        {

        }

        public S5RoleMessagingData(Guid from, Guid to, string fromRole, string toRole, string action, string valueType, string value)
        {
            FromNode = from;
            ToNode = to;
            Action = action;
            ValueType = valueType;
            Value = value;
        }
        public string FromRole { get; set; }
        public string ToRole { get; set; }

        public Guid FromNode { get; set; }
        public Guid ToNode { get; set; }

        public string Action { get; set; }

        public string ValueType { get; set; }
        public string Value { get; set; }
    }
}