﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core.Messages;

namespace Rikka.S5.S5Core
{

    /// <summary>
    /// информация текущего узла
    /// </summary>
    public class NodeInstanceInformarion
    {
        public Guid NodeId { get; set; }
        public string Address { get; set; }
        public Dictionary<string,AppDomain> Roles { get; set; }
        public NetworkConfiguration Configuration { get; set; }
        public Guid Controller { get; set; }
        public string ControllerAddress { get; set; }
    }

    /// <summary>
    /// информация о ДРУГИХ узлах
    /// </summary>
    public class NodeInformation
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string[] Roles { get; set; }
    }

    /// <summary>
    /// интерфейс определяет контекст для взаимодействия роли с текущим узлом
    /// </summary>
    public interface IS5RoleContext
    {
        //IS5RoleSyncData Data { get; }

        NodeInstanceInformarion NodeInstanceInformarion { get; }
        Task<NodeInformation[]> GetNodesInformation();

        Guid RoleId { get; }

        INodeApi Api { get; }
        INodeLogger Logger { get; }
        string RootDirectory { get; }
        string RoleDirectory { get; }
        string RoleUserDirectory { get; }
    }

    /// <summary>
    /// интерфейс определяет функционал предоставляемый роли узлом
    /// </summary>
    public interface INodeApi
    {
        Task<S5RoleMessagingDataResult> SendMessage(S5RoleMessagingData msg);
        NodeInstanceInformarion GetInstance();
        string GetProperty(string key);
        void SetProperty(string key,string value);
    }

    /// <summary>
    /// интерфейс логирования
    /// </summary>
    public interface INodeLogger
    {
        void Log(string level, string subject, string message);
        void Info(string subject, string message);
        void Warn(string subject, string message);
        void Error(string subject, string message);
    }

    public class NetworkConfiguration
    {
        public string Name { get; set; }
    }

    /// <summary>
    /// структура для хранения данных необходимых для работы контролера сети
    /// </summary>
    public class S5Controller
    {
        public string Address { get; set; }
        public List<NodeInformation> Nodes { get; set; }
        public NetworkConfiguration Configuration { get; set; }
    }


    public interface IS5RoleMessaheHandler
    {
        Task<S5RoleMessagingDataResult> Message(S5RoleMessagingData msg);
    }
    /// <summary>
    /// интерфейс определяет роль запускаемую на узлах
    /// </summary>
    public interface I5Role: IS5RoleMessaheHandler
    {
        string RoleVersion { get; set; }
        string RoleName { get; }

        void Start(IS5RoleContext instance);
        void Stop();

        S5RoleStatus RoleStatus { get; }

        void Install(IS5RoleContext instance);
        void Uninstall(IS5RoleContext instance);

    }

    /// <summary>
    /// абстрактный класс клиента роли, для облегчения взаимодействия роли с другими ролями и системами
    /// </summary>
    public abstract class S5RoleClient
    {
        protected readonly string ClientRole;
        protected readonly string ServiceRole;
        protected readonly INodeApi Api;
        protected readonly Guid NodeId;

        protected S5RoleClient(string clientRole,string serviceRole,INodeApi api)
        {
            ClientRole = clientRole;
            ServiceRole = serviceRole;
            Api = api;
            NodeId = api.GetInstance().NodeId;
        }

        protected async Task<TResult> Send<TSend, TResult>(Guid to, TSend data, string action, string valueType, string resultType) where TResult : class
        {
            var value = SerializationExt.Serialize<TSend>(data);
            var msg = new S5RoleMessagingData(NodeId, to,ClientRole,ServiceRole, action, valueType, value);
            var resultMsg = await Api.SendMessage(msg);
            if (resultMsg?.Value != null)
            {
                if (resultMsg.ValueType != resultType)
                {
                    throw new Exception();
                }
                var resultSer = new XmlSerializer(typeof(TResult));
                TResult result = default(TResult);
                using (var stream = new MemoryStream())
                {
                    var buffer = Encoding.UTF8.GetBytes(resultMsg.Value);
                    stream.Write(buffer, 0, buffer.Length);
                    result = resultSer.Deserialize(stream) as TResult;
                }
                return result;
            }
            return null;
        }

        protected async Task<TResult> Send<TResult>(Guid to, string action, string resultType) where TResult : class
        {            
            var msg = new S5RoleMessagingData(NodeId, to, action,ClientRole,ServiceRole, null, null);
            var resultMsg = await Api.SendMessage(msg);
            if (resultMsg?.Value != null)
            {
                if (resultMsg.ValueType != resultType)
                {
                    throw new Exception();
                }
                var resultSer = new XmlSerializer(typeof(TResult));
                TResult result = default(TResult);
                using (var stream = new MemoryStream())
                {
                    var buffer = Encoding.UTF8.GetBytes(resultMsg.Value);
                    stream.Write(buffer, 0, buffer.Length);
                    result = resultSer.Deserialize(stream) as TResult;
                }
                return result;
            }
            return null;
        }
    }


    //public interface IS5RoleSyncData
    //{
    //    void Sync();
    //    bool Ready { get; }
    //    List<S5RoleParameter> Parameters { get; set; }
    //}

   
    //public class S5RoleParameter
    //{
    //    public string Parameter { get; set; }
    //    public string ParameterType { get; set; }
    //    public string ValueType { get; set; }
    //    public string Value { get; set; }
    //}

    public enum S5RoleStatus
    {
        Installing,
        Ready,
        Starting,
        Started,
        Working,
        Stoping,
        Stoped,
        Uninstalling
    }
    public enum SoapVersionType
    {
        Soap11
        , Soap11WSAddressing10
        , Soap11WSAddressingAugust2004
        , Soap12
        , Soap12WSAddressing10
        , Soap12WSAddressingAugust2004
    }
    public enum WfDataType
    {
        Boolean,
        Text,
        Int,
        Float,
        Double,
        Decimal,
        Date
    }


    /// <summary>
    /// статический класс для функций сериализации и десериализации
    /// </summary>
    public static class SerializationExt
    {
        public static string Serialize<T>(T obj)
        {
            string result = null;
            var ser = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream())
            {
                ser.Serialize(stream, obj);
                result = Encoding.UTF8.GetString(stream.GetBuffer());
            }
            return result;
        }

        public static T DeserializeDeclared<T>(Stream str, string dataType)
        {
            var info = typeof (T);
            var root = info.GetCustomAttribute(typeof (XmlRootAttribute)) as XmlRootAttribute;
            if (root != null)
            {
                var type = root.Namespace + "#" + info.Name;
                if (dataType == type)
                {
                    return Deserialize<T>(str);
                } 
            }
            throw new ArgumentException();
        }
        public static T DeserializeDeclared<T>(string str,string dataType)
        {
            object result = null;
            using (var stream = new MemoryStream())
            {
                var buff = Encoding.UTF8.GetBytes(str);
                stream.Write(buff, 0, buff.Length);
                result = DeserializeDeclared<T>(stream,dataType);
            }
            return (T)result;
        }

        public static T Deserialize<T>(string str)
        {            
            object result = null;
            using (var stream = new MemoryStream())
            {
                var buff = Encoding.UTF8.GetBytes(str);
                stream.Write(buff, 0, buff.Length);
                result = Deserialize<T>(stream);
            }
            return (T)result;
        }

        public static T Deserialize<T>(Stream stream)
        {
            var ser = new XmlSerializer(typeof(T));
            object result = null;
            result = ser.Deserialize(stream);
            return (T)result;
        }

    }


    public static class DataTypes
    {

        public const string Boolean = "http://www.w3.org/2001/XMLSchema#boolean";

        public const string Integer = "http://www.w3.org/2001/XMLSchema#integer";

        public const string String = "http://www.w3.org/2001/XMLSchema#string";

        public const string Float = "http://www.w3.org/2001/XMLSchema#float";

        public const string Date = "http://www.w3.org/2001/XMLSchema#date";

        public const string DateTime = "http://www.w3.org/2001/XMLSchema#dateTime";

        /// <summary>
        /// urn - специальный URI для оконкретизации по хэшсумме/идентификатору и пр.
        /// </summary>
        public const string UrnBase = "https://tools.ietf.org/html/rfc2141";

        /// <summary>
        /// идентификация по UUID и GUID
        /// </summary>
        public const string UrnUuid = "https://tools.ietf.org/html/rfc2141#uuid";

        /// <summary>
        /// идентификация по хэшсумме MD5
        /// </summary>
        public const string UrnMd5 = "https://tools.ietf.org/html/rfc2141#md5";
    }
}