﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rikka.S5.S5Core;

namespace Rikka.S5.Wf.WsCoreClient
{
    
    public class WebServiceRoleClient : S5RoleClient
    {
        public WebServiceRoleClient(INodeApi api,string clientRole) : base(clientRole, "WebServiceSpRole", api)
        {
        }


        public const string CreateWsClientAction= "CreateWsClient";
        public const string CreateWsClientValueType= "Rikka.S5.Wf.Core.FlowEngine#CreateWsData";
        public const string CreateWsClientResultValueType= "Rikka.S5.Wf.Core.FlowEngine#WsProjectData";
        public async Task<WsProjectData> CreateWsClient(Guid machine, CreateWsData data)
        {
            var result = await Send<CreateWsData, WsProjectData>(machine, data, CreateWsClientAction,
                CreateWsClientValueType, CreateWsClientResultValueType);
            return result;
        }

        public const string CompileWsClientAction = "CompileWsClient";
        public const string CompileWsClientValueType = "Rikka.S5.Wf.Core.FlowEngine#WsProject";
        public const string CompileWsClientResultValueType = "Rikka.S5.Wf.Core.FlowEngine#WsProjectData";
        public async Task<WsProjectData> CompileWsClient(Guid machine, WsProject data)
        {
            var result = await Send<WsProject, WsProjectData>(machine, data, CompileWsClientAction,
                CompileWsClientValueType, CompileWsClientResultValueType);
            return result;
        }

        public const string GetWsClientsAction = "CompileClient";
        public const string GetWsClientsResultValueType = "Rikka.S5.Wf.Core.FlowEngine#WsClientList";
        public async Task<WsClientList> GetWsClients(Guid machine)
        {
            var result = await Send<WsClientList>(machine, GetWsClientsAction, GetWsClientsResultValueType);
            return result;
        }
    }

    public class WsExecOperation
    {
        public Guid Id { get; set; }
        public List<WsParameter> Parameters { get; set; }
    }

    public class WsParameter
    {
        public string Binding { get; set; }
        public string Parameter { get; set; }
        public WfDataType Type { get; set; }
        public string Value { get; set; }
    }    

    [XmlRoot(ElementName = "WsProjectData", Namespace = "urn:Rikka.S5.Wf.Core.FlowEngine")]
    public class WsProjectData
    {
        public string Log { get; set; }
        public string FullLog { get; set; }
        public bool Result { get; set; }
        public bool Ready { get; set; }
                
        public Guid Id { get; set; }
        public WsProject Project { get; set; }
    }

    [XmlRoot(ElementName = "WsProject", Namespace = "urn:Rikka.S5.Wf.Core.FlowEngine")]
    public class WsProject
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<KeyContentPair> Files { get; set; }
    }

    [XmlRoot(ElementName = "WWsClientList", Namespace = "urn:Rikka.S5.Wf.Core.FlowEngine")]
    public class WsClientList
    {
        public WsProjectInfo[] Items { get; set; }
    }
    public class WsProjectInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
    public class KeyContentPair
    {
        public string Key { get; set; }
        public string Content { get; set; }
    }
    

    [XmlRoot(ElementName = "CreateWsData", Namespace = "urn:Rikka.S5.Wf.Core.FlowEngine")]
    public class CreateWsData
    {
        public string Wsdl { get; set; }
        public SoapVersionType SoapVersion { get; set; }
        public string PortName { get; set; }
        public string MessageName { get; set; }
        public string Endpoint { get; set; }
    }    
}
